## MySkills

### Language

[![Language](https://skillicons.dev/icons?i=py,go,html,css,js,ts,dart)](https://skillicons.dev)

### Framework

[![Framework](https://skillicons.dev/icons?i=django,bootstrap,tailwind,svelte,flutter)](https://skillicons.dev)

### Tool

[![Tool](https://skillicons.dev/icons?i=gitlab,pycharm,docker,postgres)](https://skillicons.dev)